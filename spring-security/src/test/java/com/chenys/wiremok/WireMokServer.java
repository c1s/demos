package com.chenys.wiremok;

import com.github.tomakehurst.wiremock.client.WireMock;
import org.apache.http.impl.conn.Wire;

public class WireMokServer {

    public static void main(String[] args) {
        WireMock.configureFor(8080);
        WireMock.removeAllMappings();
        WireMock.stubFor(WireMock.get(WireMock.urlPathEqualTo("/order/1")).willReturn(WireMock.aResponse().withBody("kk")));
    }
}
