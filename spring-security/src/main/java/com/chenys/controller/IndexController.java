package com.chenys.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController {

    @GetMapping("index")
    @ApiOperation(value = "首页")
    public String index(){
        return "34";
    }
}
